// THIS IS A GENERATED FILE. DO NOT EDIT. SOURCE metrics.py AND metrics_table.py
static const oid10 questionsOID = {RECURSOR_STATS_OID, 1};
static const oid10 ipv6QuestionsOID = {RECURSOR_STATS_OID, 2};
static const oid10 tcpQuestionsOID = {RECURSOR_STATS_OID, 3};
static const oid10 cacheHitsOID = {RECURSOR_STATS_OID, 4};
static const oid10 cacheMissesOID = {RECURSOR_STATS_OID, 5};
static const oid10 cacheEntriesOID = {RECURSOR_STATS_OID, 6};
static const oid10 cacheBytesOID = {RECURSOR_STATS_OID, 7};
static const oid10 packetcacheHitsOID = {RECURSOR_STATS_OID, 8};
static const oid10 packetcacheMissesOID = {RECURSOR_STATS_OID, 9};
static const oid10 packetcacheEntriesOID = {RECURSOR_STATS_OID, 10};
static const oid10 packetcacheBytesOID = {RECURSOR_STATS_OID, 11};
static const oid10 mallocBytesOID = {RECURSOR_STATS_OID, 12};
static const oid10 servfailAnswersOID = {RECURSOR_STATS_OID, 13};
static const oid10 nxdomainAnswersOID = {RECURSOR_STATS_OID, 14};
static const oid10 noerrorAnswersOID = {RECURSOR_STATS_OID, 15};
static const oid10 unauthorizedUdpOID = {RECURSOR_STATS_OID, 16};
static const oid10 unauthorizedTcpOID = {RECURSOR_STATS_OID, 17};
static const oid10 tcpClientOverflowOID = {RECURSOR_STATS_OID, 18};
static const oid10 clientParseErrorsOID = {RECURSOR_STATS_OID, 19};
static const oid10 serverParseErrorsOID = {RECURSOR_STATS_OID, 20};
static const oid10 tooOldDropsOID = {RECURSOR_STATS_OID, 21};
static const oid10 answers01OID = {RECURSOR_STATS_OID, 22};
static const oid10 answers110OID = {RECURSOR_STATS_OID, 23};
static const oid10 answers10100OID = {RECURSOR_STATS_OID, 24};
static const oid10 answers1001000OID = {RECURSOR_STATS_OID, 25};
static const oid10 answersSlowOID = {RECURSOR_STATS_OID, 26};
static const oid10 auth4Answers01OID = {RECURSOR_STATS_OID, 27};
static const oid10 auth4Answers110OID = {RECURSOR_STATS_OID, 28};
static const oid10 auth4Answers10100OID = {RECURSOR_STATS_OID, 29};
static const oid10 auth4Answers1001000OID = {RECURSOR_STATS_OID, 30};
static const oid10 auth4AnswersSlowOID = {RECURSOR_STATS_OID, 31};
static const oid10 auth6Answers01OID = {RECURSOR_STATS_OID, 32};
static const oid10 auth6Answers110OID = {RECURSOR_STATS_OID, 33};
static const oid10 auth6Answers10100OID = {RECURSOR_STATS_OID, 34};
static const oid10 auth6Answers1001000OID = {RECURSOR_STATS_OID, 35};
static const oid10 auth6AnswersSlowOID = {RECURSOR_STATS_OID, 36};
static const oid10 qaLatencyOID = {RECURSOR_STATS_OID, 37};
static const oid10 unexpectedPacketsOID = {RECURSOR_STATS_OID, 38};
static const oid10 caseMismatchesOID = {RECURSOR_STATS_OID, 39};
static const oid10 spoofPreventsOID = {RECURSOR_STATS_OID, 40};
static const oid10 nssetInvalidationsOID = {RECURSOR_STATS_OID, 41};
static const oid10 resourceLimitsOID = {RECURSOR_STATS_OID, 42};
static const oid10 overCapacityDropsOID = {RECURSOR_STATS_OID, 43};
static const oid10 policyDropsOID = {RECURSOR_STATS_OID, 44};
static const oid10 noPacketErrorOID = {RECURSOR_STATS_OID, 45};
static const oid10 dlgOnlyDropsOID = {RECURSOR_STATS_OID, 46};
static const oid10 ignoredPacketsOID = {RECURSOR_STATS_OID, 47};
static const oid10 maxMthreadStackOID = {RECURSOR_STATS_OID, 48};
static const oid10 negcacheEntriesOID = {RECURSOR_STATS_OID, 49};
static const oid10 throttleEntriesOID = {RECURSOR_STATS_OID, 50};
static const oid10 nsspeedsEntriesOID = {RECURSOR_STATS_OID, 51};
static const oid10 failedHostEntriesOID = {RECURSOR_STATS_OID, 52};
static const oid10 concurrentQueriesOID = {RECURSOR_STATS_OID, 53};
static const oid10 securityStatusOID = {RECURSOR_STATS_OID, 54};
static const oid10 outgoingTimeoutsOID = {RECURSOR_STATS_OID, 55};
static const oid10 outgoing4TimeoutsOID = {RECURSOR_STATS_OID, 56};
static const oid10 outgoing6TimeoutsOID = {RECURSOR_STATS_OID, 57};
static const oid10 tcpOutqueriesOID = {RECURSOR_STATS_OID, 58};
static const oid10 allOutqueriesOID = {RECURSOR_STATS_OID, 59};
static const oid10 ipv6OutqueriesOID = {RECURSOR_STATS_OID, 60};
static const oid10 throttledOutqueriesOID = {RECURSOR_STATS_OID, 61};
static const oid10 dontOutqueriesOID = {RECURSOR_STATS_OID, 62};
static const oid10 unreachablesOID = {RECURSOR_STATS_OID, 63};
static const oid10 chainResendsOID = {RECURSOR_STATS_OID, 64};
static const oid10 tcpClientsOID = {RECURSOR_STATS_OID, 65};
#ifdef __linux
static const oid10 udpRecvbufErrorsOID = {RECURSOR_STATS_OID, 66};
#endif
#ifdef __linux
static const oid10 udpSndbufErrorsOID = {RECURSOR_STATS_OID, 67};
#endif
#ifdef __linux
static const oid10 udpNoportErrorsOID = {RECURSOR_STATS_OID, 68};
#endif
#ifdef __linux
static const oid10 udpInErrorsOID = {RECURSOR_STATS_OID, 69};
#endif
static const oid10 ednsPingMatchesOID = {RECURSOR_STATS_OID, 70};
static const oid10 ednsPingMismatchesOID = {RECURSOR_STATS_OID, 71};
static const oid10 dnssecQueriesOID = {RECURSOR_STATS_OID, 72};
static const oid10 nopingOutqueriesOID = {RECURSOR_STATS_OID, 73};
static const oid10 noednsOutqueriesOID = {RECURSOR_STATS_OID, 74};
static const oid10 uptimeOID = {RECURSOR_STATS_OID, 75};
static const oid10 realMemoryUsageOID = {RECURSOR_STATS_OID, 76};
static const oid10 fdUsageOID = {RECURSOR_STATS_OID, 77};
static const oid10 userMsecOID = {RECURSOR_STATS_OID, 78};
static const oid10 sysMsecOID = {RECURSOR_STATS_OID, 79};
static const oid10 dnssecValidationsOID = {RECURSOR_STATS_OID, 80};
static const oid10 dnssecResultInsecureOID = {RECURSOR_STATS_OID, 81};
static const oid10 dnssecResultSecureOID = {RECURSOR_STATS_OID, 82};
static const oid10 dnssecResultBogusOID = {RECURSOR_STATS_OID, 83};
static const oid10 dnssecResultIndeterminateOID = {RECURSOR_STATS_OID, 84};
static const oid10 dnssecResultNtaOID = {RECURSOR_STATS_OID, 85};
static const oid10 policyResultNoactionOID = {RECURSOR_STATS_OID, 86};
static const oid10 policyResultDropOID = {RECURSOR_STATS_OID, 87};
static const oid10 policyResultNxdomainOID = {RECURSOR_STATS_OID, 88};
static const oid10 policyResultNodataOID = {RECURSOR_STATS_OID, 89};
static const oid10 policyResultTruncateOID = {RECURSOR_STATS_OID, 90};
static const oid10 policyResultCustomOID = {RECURSOR_STATS_OID, 91};
static const oid10 queryPipeFullDropsOID = {RECURSOR_STATS_OID, 92};
static const oid10 truncatedDropsOID = {RECURSOR_STATS_OID, 93};
static const oid10 emptyQueriesOID = {RECURSOR_STATS_OID, 94};
static const oid10 dnssecAuthenticDataQueriesOID = {RECURSOR_STATS_OID, 95};
static const oid10 dnssecCheckDisabledQueriesOID = {RECURSOR_STATS_OID, 96};
static const oid10 variableResponsesOID = {RECURSOR_STATS_OID, 97};
static const oid10 specialMemoryUsageOID = {RECURSOR_STATS_OID, 98};
static const oid10 rebalancedQueriesOID = {RECURSOR_STATS_OID, 99};
static const oid10 qnameMinFallbackSuccessOID = {RECURSOR_STATS_OID, 100};
static const oid10 proxyProtocolInvalidOID = {RECURSOR_STATS_OID, 101};
static const oid10 recordCacheContendedOID = {RECURSOR_STATS_OID, 102};
static const oid10 recordCacheAcquiredOID = {RECURSOR_STATS_OID, 103};
static const oid10 nodLookupsDroppedOversizeOID = {RECURSOR_STATS_OID, 104};
static const oid10 taskqueuePushedOID = {RECURSOR_STATS_OID, 105};
static const oid10 taskqueueExpiredOID = {RECURSOR_STATS_OID, 106};
static const oid10 taskqueueSizeOID = {RECURSOR_STATS_OID, 107};
static const oid10 aggressiveNSECCacheEntriesOID = {RECURSOR_STATS_OID, 108};
static const oid10 aggressiveNSECCacheNSECHitsOID = {RECURSOR_STATS_OID, 109};
static const oid10 aggressiveNSECCacheNSEC3HitsOID = {RECURSOR_STATS_OID, 110};
static const oid10 aggressiveNSECCacheNSECWcHitsOID = {RECURSOR_STATS_OID, 111};
static const oid10 aggressiveNSECCacheNSEC3WcHitsOID = {RECURSOR_STATS_OID, 112};
static const oid10 dotOutqueriesOID = {RECURSOR_STATS_OID, 113};
static const oid10 dns64PrefixAnswersOID = {RECURSOR_STATS_OID, 114};
static const oid10 almostExpiredPushedOID = {RECURSOR_STATS_OID, 115};
static const oid10 almostExpiredRunOID = {RECURSOR_STATS_OID, 116};
static const oid10 almostExpiredExceptionsOID = {RECURSOR_STATS_OID, 117};
#ifdef __linux
static const oid10 udpInCsumErrorsOID = {RECURSOR_STATS_OID, 118};
#endif
#ifdef __linux
static const oid10 udp6RecvbufErrorsOID = {RECURSOR_STATS_OID, 119};
#endif
#ifdef __linux
static const oid10 udp6SndbufErrorsOID = {RECURSOR_STATS_OID, 120};
#endif
#ifdef __linux
static const oid10 udp6NoportErrorsOID = {RECURSOR_STATS_OID, 121};
#endif
#ifdef __linux
static const oid10 udp6InErrorsOID = {RECURSOR_STATS_OID, 122};
#endif
#ifdef __linux
static const oid10 udp6InCsumErrorsOID = {RECURSOR_STATS_OID, 123};
#endif
static const oid10 sourceDisallowedNotifyOID = {RECURSOR_STATS_OID, 124};
static const oid10 zoneDisallowedNotifyOID = {RECURSOR_STATS_OID, 125};
static const oid10 nonResolvingNameserverEntriesOID = {RECURSOR_STATS_OID, 126};
static const oid10 maintenanceUsecOID = {RECURSOR_STATS_OID, 127};
static const oid10 maintenanceCallsOID = {RECURSOR_STATS_OID, 128};
static const oid10 authNoerrorAnswersOID = {RECURSOR_STATS_OID, 129};
static const oid10 authFormerrAnswersOID = {RECURSOR_STATS_OID, 130};
static const oid10 authServfailAnswersOID = {RECURSOR_STATS_OID, 131};
static const oid10 authNxdomainAnswersOID = {RECURSOR_STATS_OID, 132};
static const oid10 authNotimpAnswersOID = {RECURSOR_STATS_OID, 133};
static const oid10 authRefusedAnswersOID = {RECURSOR_STATS_OID, 134};
static const oid10 authYxdomainAnswersOID = {RECURSOR_STATS_OID, 135};
static const oid10 authYxrrsetAnswersOID = {RECURSOR_STATS_OID, 136};
static const oid10 authNxrrsetAnswersOID = {RECURSOR_STATS_OID, 137};
static const oid10 authNotauthAnswersOID = {RECURSOR_STATS_OID, 138};
static const oid10 authRcode10AnswersOID = {RECURSOR_STATS_OID, 139};
static const oid10 authRcode11AnswersOID = {RECURSOR_STATS_OID, 140};
static const oid10 authRcode12AnswersOID = {RECURSOR_STATS_OID, 141};
static const oid10 authRcode13AnswersOID = {RECURSOR_STATS_OID, 142};
static const oid10 authRcode14AnswersOID = {RECURSOR_STATS_OID, 143};
static const oid10 authRcode15AnswersOID = {RECURSOR_STATS_OID, 144};
static const oid10 packetcacheContendedOID = {RECURSOR_STATS_OID, 145};
static const oid10 packetcacheAcquiredOID = {RECURSOR_STATS_OID, 146};
static const oid10 nodEventsOID = {RECURSOR_STATS_OID, 147};
static const oid10 udrEventsOID = {RECURSOR_STATS_OID, 148};
static const oid10 maxChainLengthOID = {RECURSOR_STATS_OID, 149};
static const oid10 maxChainWeightOID = {RECURSOR_STATS_OID, 150};
static const oid10 chainLimitsOID = {RECURSOR_STATS_OID, 151};
static const oid10 tcpOverflowOID = {RECURSOR_STATS_OID, 152};
